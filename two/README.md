# Problem Set 2
## Behavioral Design Pattern

You are asked to improve the design of a simple `FortuneWheel` game.
Below are brief explanations for each behavior that can be performed in FortuneWheel simulation:
1. rollTheWheel(int guessedNumber): Player will call this method to start the game by spinning the wheel while guessing which number will be shown after the wheel stopped spinning. The player will choose between 0-9.
2. instantStop(): Player will call this method to stop the wheel immediately.
3. stopSlowly(): Player will call this method to stop the wheel after the count of three.
4. rollingResult(): Player can check which number pointed by the wheel after it stopped completely.

The owner of the `FortuneWheel` worries with the current design.
To make the game more interesting, he decided to change the behavior of the `FortuneWheel`. 
Unfortunately, the design of `FortuneWheel` class is cluttered with branching statements. 
The owner worries that if he try to add new behavior using current design, it will create an unknown side effect. 
**You are asked to refactor the FortuneWheel class using your knowledge of Design Principles and Patterns**.

### How to use

```java
javac FortuneWheelSimulation.java
java FortuneWheelSimulation
```

### Questions

1. Which Design Principles violated the above code? Explain your reasoning for each violated Design Principles you mentioned!
```
The Above Code violates Single Responsibility Principle and needs to be fixed with State Pattern. For example, this can
be seen in rollingResult() method, in this method, there are multiple state that "repeatable" in other methods such as whether the wheel is rolling, idle, etc. 
This can be fixed by implementing State Pattern and create multiple different state behaviours respectively.
```
2. Choose one of the Behavioral Design Patterns that is most suitable to improve the above code? Explain the reason why that pattern is most suitable using 1 - 3 sentences!
```
Behavioral Design Pattern that is most suitable to improve the above code is State Pattern. Because if we look at the code above,
each method contains multiple state, and needs to be refactored into different state behaviours respectively
```
3. Apply refactoring steps to the above code snippet based on your chosen pattern from Question number 2! 
**Make sure that each refactoring step is illustrated using one git commit.**
```
Intinya buat abstract state yang isinya state - state Wheel Rolling,Idle,Stop,sama Stopping Slowly. lalu masing2 state dibuat classnya
dan meng-extends abstract state tersebut
```

*note , saya sudah commit jam 5:03 seperti ini , tetapi ketika dilihat tidak ada dan commit hanya readme.md
jadi saya commit dan push ulang screenshot saya lampirkan di scele